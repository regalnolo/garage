// JavaScript source code
//console.log("je suis le serveur");
let http = require("http");
// on crée le serveur // renvoie un callback avec deux param la requete et la reponse
//let server = http.createServer((requete, response) => {
//    response.writeHead(200); // tout s'est bien passé
//    response.end ('La requete s\'est bien terminee.')
//});

// pour utiliser express
const express = require('express');
const app = express();

const port = 3000
const bodyParser = require('body-parser')
// pour autoriser les sites à appeler l'api
const cors = require('cors')
app.use(cors())

app.use(
    bodyParser.urlencoded({
        extended: true
    }));
    
app.use(bodyParser.json());

/*Connexion singleton à la base de données mySql*/

function createMySqlConnection(connection) {
    if (connection == null) {
        var mysql = require('mysql')
        var connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: 'Norick09Loris12',
            database: 'nodecars'
        });
    } else {
        connection = connection
    }
    return connection;
}

var connection = createMySqlConnection(connection);
connection.connect()

// la meme chose que la zone commentaire avec le framework express
//app.get('/', (req, res) => res.send('Hello World!'));

app.get('/voitures', (req, res) => {
  ///* pour remplacer cors : protection contre intrusion d'un client quelconque qui souhaiterait récupérer les données '*' pour l'url http://localhost:3000*/
  //res.header('Access-Control-Allow-Origin',  '*');

    connection.query('SELECT * FROM cars', function (err, rows, fields) {
        if (err) throw err

        console.log('Liste des voitures : ', rows)
        res.json(rows);
    })

    //connection.end()

});


/*ajoute une voiture à la bd*/

app.post('/voitures', (req, res) => {
    console.log(req.body.voiture);
    const modele = req.body.voiture.modele;
    const couleur = req.body.voiture.couleur;
    const marque = req.body.voiture.marque;
    const imagelink = req.body.voiture.imagelink;

    connection.query(`INSERT INTO cars (id, marque, modele, couleur, imagelink) VALUES (null, '${marque}', '${modele}','${couleur}', '${imagelink}')`, function (err, rows, fields) {
        if (err) throw err

        console.log('Liste des voitures insérées en bdd : ', rows)
        res.json(rows);
    }
    );
});





/*methode de suppression, récupérer l'id du front*/
app.post('/deletevoiture', (req, res) => {
    // voiture à supprimer
    console.log(req.body.voiture.id);
    const id = req.body.voiture.id;
   
    connection.query(`DELETE FROM cars WHERE id= '${id}'`, function (err, rows, fields) {
        if (err) throw err
        console.log(rows)
        res.json(rows);
    }
    );
});

// récupère la voiture dont l'id est passé en param
app.post('/voiturebyid', (req, res) => {

  id = req.body.voiture.id;
  connection.query(`SELECT * FROM cars WHERE id='${id}'`, function (err, rows, fields) {
    if (err) throw err

    console.log('Car to update : ', rows)
    res.json(rows);
  })

  //connection.end()

});



/*Mise à jour d'une voiture */
app.post('/updatevoiture', (req, res) => {
    console.log(req.body.voiture);
    const id = req.body.voiture.id;
    const modele = req.body.voiture.modele;
    const couleur = req.body.voiture.couleur;
    const marque = req.body.voiture.marque;
    const imagelink = req.body.voiture.imagelink;

    connection.query(`UPDATE cars SET id='${id}',marque='${marque}',modele='${modele}',couleur='${couleur}',imagelink= '${imagelink}' where id='${id}'`, function (err, rows, fields) {
        if (err) throw err

        console.log('voiture updatee : ', rows)
        res.json(rows);
    }
    );
});

// si mauvaise requete
app.use((req, res) => res.sendStatus(404));


app.listen(port, () => console.log(`Example app listening on port ${port}!`));

