import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GarageService {

  constructor(private httpClient: HttpClient) { }

  // on récupère la liste des voiture de l'api de Node :  on reçoit une réponse JSON
  getCars = () => {
    console.log('on appelle getVoitures ok');
    var result = this.httpClient.get('http://localhost:3000/voitures');
    console.log(result);
    return this.httpClient.get('http://localhost:3000/voitures');
  };

  // pour préremplir l'update d'une voiture
  getCarById = (id: number) => {
    console.log('on appelle getVoitures ok');
    var result = this.httpClient.post('http://localhost:3000/voiturebyid', { id: id });
    console.log(result);
    return result;
  };


  addCar = (auto: any) => {
    console.log('on appelle addCar ok');
    var result = this.httpClient.post('http://localhost:3000/voitures', { auto: auto});
    console.log(result);
    return result;
  };


  
  // on supprime l'id de la voiture passé en paramètre
  deleteCar = (id) => {
    return this.httpClient.post('http://localhost:3000/deletevoiture', { id: id });
   
  };


  // on supprime l'id de la voiture passé en paramètre
  updateCar = (id) => {
    return this.httpClient.post('http://localhost:3000/updatevoiture', { id: id });
  };
}
