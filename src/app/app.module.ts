import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GarageComponent } from './garage/garage.component';

import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
// import HttpClientModule pour utliser le service de com vers l'api
import { HttpClientModule } from '@angular/common/http';
import { UpdatecarComponent } from './updatecar/updatecar.component';
import { CreatecarComponent } from './createcar/createcar.component';


const routes: Routes = [
  { path: 'garage', component: GarageComponent },
  { path: 'updatecar', component: UpdatecarComponent },
  { path: 'createcar', component: CreatecarComponent },
  { path: 'updatecar/:id', component: CreatecarComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    GarageComponent,
    HeaderComponent,
    FooterComponent,
    UpdatecarComponent,
    CreatecarComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
