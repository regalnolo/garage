import { Component, OnInit } from '@angular/core';
import { GarageService } from '../services/garage.service'
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-garage',
  templateUrl: './garage.component.html',
  styleUrls: ['./garage.component.css']
})
export class GarageComponent implements OnInit {
  // propriétés
  carsList = [];
  cars: any;
  filteredCars = {};
  car: any;
  auto: any;
  constructor(private garageService: GarageService, private router: Router, private activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {

    this.garageService.getCars().subscribe(
      // si data on les affiche
      (RowDataPacket) => {
        this.cars = RowDataPacket;
        this.filteredCars = this.cars;
        console.log(this.cars);
      },
      // sinon on affiche l'erreur
      err => {
        console.log(err);
      }
    );
  }


  // filtre les voitures par marque
  onSearch = (marque) => {
    console.log(marque.value);

    // ne fonctionne pas sur {}
    this.filteredCars = this.cars.filter(
      (car) => car['marque'].search(marque.value) > -1
    );
    console.log(this.filteredCars);
    marque.value = "";
  };

  /*sur clic btnSuppr*/
  onDelete = (event, id) => {
    event.preventDefault();
    console.log(id.value);
    if (id != null) {
      this.garageService.deleteCar(id).subscribe((data) => {
        console.log(data);
        this.cars = data;
      }, (err) => {
        console.error(err);
      });
    }
  }
  /*Clic sur un modele à modifier, on récupère l'id à modifier et redirection vers update*/
  onUpdate = (event, id) => {
    event.preventDefault();
    console.log(id.value);
    if (id != null) {
      this.garageService.updateCar(id).subscribe((data) => {
        console.log(data);
        this.cars = data;
      }, (err) => {
        console.error(err);
      });
    }
  }


  // clic sur edit

  onClick = (event, ref) => {
    event.preventDefault();
    console.log(ref.innerHTML);
    if (ref != null) {
      console.log(this.activatedRoute.snapshot.params.id);

      this.garageService.getCarById(ref.innerHTML).subscribe
      {
        data => console.log(data => {
          console.log(data);
          this.auto = data[0];
          this.router.navigate(["/updatecar"]);
        },
          err => {
            console.error(err);
            //
          }
        );
      }

    }

  }
}
