import { Component, OnInit } from '@angular/core';
import { GarageService } from '../services/garage.service';

import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-createcar',
  templateUrl: './createcar.component.html',
  styleUrls: ['./createcar.component.css']
})
export class CreatecarComponent implements OnInit {

  constructor(private garageService: GarageService, private routeur: Router, private activatedRoute: ActivatedRoute) { }
  auto: any;

  ngOnInit() {

  }



  onSubmit(event, marque, modele, couleur, km, nbdoors, date, ctrltec) {
   

      event.preventDefault();

      console.log(marque.value);
      console.log(modele.value);
      console.log(couleur.value);
      console.log(km.value);
      console.log(nbdoors.value);
      console.log(date.value);
      console.log(ctrltec.value);
      const auto = {
        marque: marque.value,
        modele: modele.value,
        couleur: couleur.value,
        km: km.value,
        nbdoors: +nbdoors.value, //+ nombre 
        date: +date.value,
        ctrltec: +ctrltec.value

      };
      console.log(auto);
      this.garageService.addCar(auto).subscribe(data => {
        console.log(data);
        this.routeur.navigate(["/garage"]);
      },
        err => {
          console.log(err);
          //
        }
      );
    };
  }

