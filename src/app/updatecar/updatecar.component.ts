import { Component, OnInit } from '@angular/core';
import { GarageService } from '../services/garage.service';



@Component({
  selector: 'app-updatecar',
  templateUrl: './updatecar.component.html',
  styleUrls: ['./updatecar.component.css']
})
export class UpdatecarComponent implements OnInit {

  constructor(private garageService: GarageService) { }

  ngOnInit() {



  }


  onSubmit(event, marque, modele, couleur, km, portes, date, ctrltec) {

    event.preventDefault;
    
    console.log(marque);
    console.log(modele);
    console.log(couleur);
    console.log(km);
    console.log(portes);
    console.log(date);
    console.log(ctrltec);
    const auto = {
      marque: marque.value,
      modele: modele.value,
      couleur: couleur.value,
      km: km.value,
      portes: portes.value,
      date: date.value,
      ctrltec: ctrltec.value

    };
    console.log(auto);
    this.garageService.addCar(auto).subscribe;
  }
}
